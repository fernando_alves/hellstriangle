# frozen_string_literal: true

class Triangle
  attr_reader :root
  def initialize(triangle_description)
    @root = Node.new(triangle_description[0][0])
    assign_neighbors(@root, 0, 0, triangle_description)
  end

  def self.validate(triangle_description)
    return false if triangle_description.empty?
    return false unless triangle_description.all? { |row| row.is_a? Array }
    return false unless triangle_description.flatten.all? { |e| e.is_a? Numeric }
    triangle_description.each_cons(2) do |consecutive_rows|
      return false unless consecutive_rows[1].length == consecutive_rows[0].length + 1
    end
    true
  end

  private

  def assign_neighbors(node, node_position, row_index, triangle_description)
    return unless any_neighbor?(row_index, triangle_description)
    assign_left_neighbor(node, node_position, row_index, triangle_description)
    assign_right_neighbor(node, node_position, row_index, triangle_description)
  end

  def assign_left_neighbor(node, node_position, row_index, triangle_description)
    neighbor = Node.new(triangle_description[row_index + 1][node_position])
    node.left = neighbor
    assign_neighbors(neighbor, node_position, row_index + 1, triangle_description)
  end

  def assign_right_neighbor(node, node_position, row_index, triangle_description)
    neighbor = Node.new(triangle_description[row_index + 1][node_position + 1])
    node.right = neighbor
    assign_neighbors(neighbor, node_position + 1, row_index + 1, triangle_description)
  end

  def any_neighbor?(row_index, triangle_description)
    triangle_description[row_index + 1]
  end
end
