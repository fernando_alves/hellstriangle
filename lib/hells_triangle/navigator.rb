# frozen_string_literal: true

class Navigator
  class << self
    def maximum_value_of(triangle)
      triangle.root.value + maximum_value_among_neighbors(triangle.root)
    end

    private

    def maximum_value_among_neighbors(root)
      return 0 unless root.left && root.right
      left_value = root.left.value + maximum_value_among_neighbors(root.left)
      right_value = root.right.value + maximum_value_among_neighbors(root.right)
      [left_value, right_value].max
    end
  end
end
