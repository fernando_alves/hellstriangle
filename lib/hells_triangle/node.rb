# frozen_string_literal: true

class Node
  attr_reader :value
  attr_accessor :left, :right
  def initialize(value, left = nil, right = nil)
    @value = value
    @left = left
    @right = right
  end

  def ==(other)
    return false unless other.is_a? Node
    @value == other.value
  end
end
