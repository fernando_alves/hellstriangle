# frozen_string_literal: true

require 'yaml'

class InputParserError < StandardError; end

class InputParser
  class << self
    def parse(input)
      YAML.safe_load(input)
    rescue Psych::SyntaxError => e
      raise InputParserError, e.message
    end

    def validate(input)
      return false unless input.respond_to? :empty?
      return false if input.empty?
      return false unless parse(input).is_a? Array
      true
    rescue InputParserError
      return false
    end
  end
end
