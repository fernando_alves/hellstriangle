# frozen_string_literal: true

require_relative './hells_triangle/navigator'
require_relative './hells_triangle/node'
require_relative './hells_triangle/triangle'
require_relative './hells_triangle/input_parser'
