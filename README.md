# HellsTriangle

## Por que ruby?

Sinceramente, faz algum tempo que não trabalho diretamente com ruby. Decidi usar esse problema como oportunidade de revisitar a linguagem e seu ecosistema.

Considero ruby especialmente rica quanto a explicitar a intenção através do código.

## Requisitos

- Ruby 2.4.1
- Bundler >= 1.15

## Desenvolvimento

### Instalando dependências

Para instalar as dependências execute `make install`.  

### Executando testes

Os testes podem ser executados através do comando `make test`.

Há 2 tipos de verificação:
  - Análise estática de código usando o [rubocop](https://github.com/bbatsov/rubocop)
  - Testes de unidade usando [minitest](https://github.com/seattlerb/minitest)

## Executando

A aplicação pode ser executada através do aquivo `/bin/hells-triangle <descrição>`. Onde a descrição deve estar no formato de um array bi-dimensional.

Por exemplo, `./bin/hells-triangle [[6],[3,5],[9,7,1],[4,6,8,4]]` resultará na saída:

> The maximum total is: 26
