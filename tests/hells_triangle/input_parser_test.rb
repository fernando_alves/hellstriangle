# frozen_string_literal: true

require 'minitest/autorun'
require_relative '../../lib/hells_triangle/input_parser'

describe InputParser do
  describe 'validating an input' do
    it 'invalidates if its empty' do
      InputParser.validate('').wont_equal true
    end

    it 'invalidates if its not an array' do
      InputParser.validate(1).wont_equal true
      InputParser.validate('a').wont_equal true
      InputParser.validate('{1}').wont_equal true
      InputParser.validate('Object.new').wont_equal true
    end

    it 'invalidates a malformed input' do
      InputParser.validate('[a').wont_equal true
    end

    it 'validates if its a string representation of array' do
      InputParser.validate('[1]').must_equal true
      InputParser.validate('[1, 3]').must_equal true
      InputParser.validate('[1, [3]]').must_equal true
    end
  end

  describe 'parsing' do
    it 'parses string representation of array into an array' do
      InputParser.parse('[1]').must_equal [1]
    end

    it 'parses string representation of multi-dimensional array into an array' do
      InputParser.parse('[1,[1,2]]').must_equal [1, [1, 2]]
    end

    it 'raises error if input cant be parsed' do
      proc { InputParser.parse('[a').must_equal [1, [1, 2]] }.must_raise InputParserError
    end
  end
end
