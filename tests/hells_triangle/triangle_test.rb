# frozen_string_literal: true

require 'minitest/autorun'
require_relative '../../lib/hells_triangle'

describe Triangle do
  describe 'when initializing' do
    it 'sets the root node as the top most element of the triangle' do
      triangle = Triangle.new([[2]])
      expected_root = Node.new(2)

      triangle.root.must_equal expected_root
    end

    it 'sets proper neighbors for all nodes' do
      root = Triangle.new([[2], [3, 4], [12, 18, 23]]).root
      root.left.must_equal Node.new(3)
      root.right.must_equal Node.new(4)

      root.left.left.must_equal Node.new(12)
      root.left.right.must_equal Node.new(18)

      root.right.left.must_equal Node.new(18)
      root.right.right.must_equal Node.new(23)

      assert_nil root.right.right.left
      assert_nil root.right.right.right
      assert_nil root.left.left.left
    end
  end

  describe 'validating' do
    it 'invalidates if theres no root' do
      Triangle.validate([]).wont_equal true
    end

    it 'validates if theres only the root' do
      Triangle.validate([[1]]).must_equal true
    end

    it 'invalidates if the row is not an array' do
      Triangle.validate([1]).wont_equal true
    end

    it 'invalidates if row is not longer than previous one' do
      Triangle.validate([[2], [3]]).wont_equal true
    end

    it 'validates if row is are 1 element longer than previous one' do
      Triangle.validate([[2], [1, 3]]).must_equal true
    end

    it 'invalidates if row is are more than 1 element longer than previous one' do
      Triangle.validate([[2], [1, 2, 3]]).wont_equal true
    end

    it 'invalidates if elements are not numbers' do
      Triangle.validate([['a']]).wont_equal true
    end
  end
end
