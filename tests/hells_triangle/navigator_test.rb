# frozen_string_literal: true

require 'minitest/autorun'
require_relative '../../lib/hells_triangle'

describe Navigator do
  describe 'when calculating maximum value of a triangle' do
    it 'uses root value if triangle has one element' do
      triangle = Triangle.new([[3]])
      Navigator.maximum_value_of(triangle).must_equal 3
    end

    it 'adds value of neighbors if there are any' do
      triangle = Triangle.new([[3], [1, 1]])
      Navigator.maximum_value_of(triangle).must_equal 4
    end

    it 'adds highest value neighbor' do
      triangle = Triangle.new([[3], [1, 3], [2, 1, 4]])
      Navigator.maximum_value_of(triangle).must_equal 10
    end
  end
end
