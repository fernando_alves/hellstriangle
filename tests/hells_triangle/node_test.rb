# frozen_string_literal: true

require 'minitest/autorun'
require_relative '../../lib/hells_triangle'

describe Node do
  describe 'when initializing' do
    it 'assigns left and right if provided' do
      left_node = Node.new(5)
      right_node = Node.new(4)
      node = Node.new(2, left_node, right_node)

      node.left.must_equal left_node
      node.right.must_equal right_node
    end
  end

  describe 'when compared to other' do
    subject { Node.new(2) }

    it 'is not equal to something that is not a Node' do
      (subject == '1').wont_equal true
      (subject == 2).wont_equal true
      (subject == Object.new).wont_equal true
    end

    it 'is not equal to a Node with different value' do
      (subject == Node.new(1)).wont_equal true
    end

    it 'is equal to a Node with same value' do
      (subject == Node.new(2)).must_equal true
    end
  end
end
