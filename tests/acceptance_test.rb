# frozen_string_literal: true

require 'minitest/autorun'
require_relative '../lib/hells_triangle'

describe 'Acceptance' do
  it 'answers correctly proposed example' do
    triangle = Triangle.new [[6], [3, 5], [9, 7, 1], [4, 6, 8, 4]]
    Navigator.maximum_value_of(triangle).must_equal 26
  end
end
